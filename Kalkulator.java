package kalukulatorsederhana;

public class Kalkulator {
    public static void main(String[] args) {

        kalkulator("2/0");
    }

    public static void kalkulator(String number) {
        String[] num = number.split("[0-9]+");
        String[] operator = number.split("[x/+-]");
        try {
            int parseInt = Integer.parseInt(operator[0]);

            try {
                for (int i = 1; i < operator.length; i++) {
                    switch (num[i]) {
                        case "+" -> parseInt += Integer.parseInt(operator[i]);
                        case "x" -> parseInt *= Integer.parseInt(operator[i]);
                        case "/" -> parseInt /= Integer.parseInt(operator[i]);
                        case "-" -> parseInt -= Integer.parseInt(operator[i]);
                    }
                    System.out.println(parseInt);
                }

            } catch (ArithmeticException e) {
                System.err.println("tidak bisa dilakukan");
            }
        } catch (NumberFormatException e) {
            System.err.println("Tidak boleh ada spasi");
        }

    }
}
